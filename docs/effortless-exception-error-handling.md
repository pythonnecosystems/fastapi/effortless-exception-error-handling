# FastAPI의 손쉬운 예외 오류 처리: 깔끔하고 간소화된 접근 방식
프로젝트를 배포한 후 예외 오류, 특히 긴 스택 추적을 포함하는 예외 오류를 효과적으로 식별하는 데 어려움을 겪는다. `debug=True`을 활성화하면 불필요한 정보로 출력이 복잡해져 프로덕션 환경에서는 바람직하지 않았기 때문에 원하는 만큼의 도움을 얻지 못했다.

이 문제를 해결하기 위해 오류를 처리하는 다양한 접근 방식을 모색한 결과, 다음과 같이 오류를 보다 간결하고 체계적으로 표시하는 미들웨어를 구현하는 훨씬 더 깔끔한 해결책을 찾았다.

![](1_7ppRaCiONMqLkOpJwZ-7dQ.webp)

FastAPI를 초기화할 때 기본값인 `debug=False`인 경우 `debug=True`로 설정되지 않아 아래같이 출력된다.

![](./1_57XlbpFERqKh4_YaIVaqiA.webp)

또는 `debug=True`로 설정하면 출력은 아래와 같다.

![](./1_jYeBE8ieWwYWCPqfDunTkQ.webp)

> Warning!<br>
> 아래 문장으로 단 하나뿐인 Chat-GPT에 의해 터보차저와 슈퍼차저를 장착했다! 이 문장이 뿜어내는 놀라움에 마음의 준비를 하시고, 이제 여러분은 인공지능이 만들어낸 뛰어난 능력을 갖추게 되었음을 기억하세요! 😎

그래서 저자는 성가신 예외 오류를 모두 정복하기 위한 고귀한 여정을 시작했고, 코드베이스를 넘쳐나도록 위협하는 지저분하고 반복적인 try-catch 블록과의 전쟁을 선포했다. 강력한 핸들러 하나로 오류 괴물들을 심연으로 추방하고 이제는 깔끔하고 우아한 코드의 달콤한 평온을 만끽하고 있다.

## 미들웨어 생성
여기서 미들웨어의 신비로운 깊이를 파헤치지는 않겠다. 궁금한 점이 있으시면 Google을 참고하세요.

이제 저자가 좋아하는 슈퍼히어로 액션 피규어처럼 깔끔하게 정리된 파일로 세심하게 만든 프로젝트의 디렉토리 구조를 공개한다. 길고 지루한 문장을 한 파일에서 읽으시나요? 

```
|- main.py
|- __init__.py
  |- middlewares
    |- __init__.py
    |- exception.py
  |- router
    | __init__.py
    |- api_v1
    |- routes.py
... more directories ...
```

다음 단계를 따르세요:

1. 프로젝트 루트 디렉토리에 `middlewares`라는 이름의 새 디렉터리를 만든다.
1. 그런 다음 `exception.py` 또는 원하는 이름의 새 파일을 만든다.
1. 그런 다음 아래 코드를 복사한다.

```python
# ./middleware/exception.py

from traceback import print_exception

from fastapi import Request
from fastapi.responses import JSONResponse
from starlette.middleware.base import BaseHTTPMiddleware

class ExceptionHandlerMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request: Request, call_next):
        try:
            return await call_next(request)
        except Exception as e:
            print_exception(e)
            return JSONResponse(
                status_code=500, 
                content={
                    'error': e.__class__.__name__, 
                    'messages': e.args
                }
            )
```

위의 코드를 살펴보자, 마치 Starlette의 최고급 갑옷을 입은 FastAPI의 수호천사 같은 존재이다! 이 고귀한 미들웨어는 두려움 없이 들어오는 요청을 가로채고, 백그라운드에서 라우트 처리기와 춤을 추며, 심지어 하모니를 방해하는 장난스러운 예외를 잡아내기도 한다. 그리고 예외가 포착되면 이 용감한 기사는 JSON 응답을 작성하여 오류의 비밀을 세상에 공개한다! 우리가 몰랐던 영웅, 예외 처리기 미들웨어에게 박수를 보낸다! 🦸‍♂️💻

이제 코드 내부의 미스터리를 장엄하게 공개할 준비를 하였다.

1. `from trackback import print_exception`: 이 임포트는 예외가 발생하면 예외 트레이스백을 인쇄하는 데 사용된다.
1. `from fastapi import Request`: HTTP 요청을 나타내는 FastAPI에서 `Request`을 임포트힌다.
1. `from fastapi.responses import JSONResponse`: FastAPI에서 JSONResponse를 임포트하여 JSON 응답을 생성하는 데 사용된다.
1. `from starlette.middleware.base import BaseHTTPMiddleware`: 커스텀 미들웨어의 base 클래스로 사용될 Starlette 미들웨어의 base 클래스를 임포트한다.
1. `class ExceptionHandlerMiddleware(BaseHTTPMiddleware)`: 커스텀 예외 처리기 미들웨어 클래스를 정의한다. `BaseHTTPMiddleware`으로부터 상속받는다.
1. `async def dispatch(self, request: Request, call_next)`: `BaseHTTPMiddleware`의 `dispatch` 메서드를 재정의(overriding)한다. 이 메서드는 요청이 수신될 때 호출된다.
1. `try...except` 블럭: `try` 블록에는 요청을 정상적으로 처리하는 동안 실행될 코드가 포함되어 있다. `except` 블록은 처리 과정에서 발생할 수 있는 모든 예외를 포착한다.
1. `print_exception(e)`: 예외가 발생하면 이 문장은 예외 트레이스백을 콘솔에 인쇄한다.
1. `JSONResponse`: 예외가 발생하면 이 문장은 상태 코드가 500(내부 서버 오류)인 JSON 응답을 생성하고 응답 본문에 오류에 대한 정보를 포함한다.
1. `'error': e.__class__.__name__`: 예외 클래스의 이름(예: 'ValueError', 'TypeError')을 JSON 응답에 포함시킨다.
1. `'messages': e.args`: 예외의 인수를 JSON 응답에 포함시킨다. 이러한 인수는 일반적으로 오류에 대한 설명과 예외가 발생할 때 전달된 추가 정보를 포함한다.

> **Note**: <br>
> *프로덕션 환경에서 콘솔에 예외를 인쇄한다고요? 이는 다과회를 알리기 위해 호루라기를 사용하는 것과 같다. 효과는 있을지 모르지만 우아하지 않다. 프로덕션 환경에서는 이러한 성가신 예외를 파일이나 모니터링 시스템에 기록하고 고양이 동영상이나 밈과 같은 중요한 작업은 콘솔에 남겨두는 것이 현명할 것이다.*

그런 다음 `main.py`에 아래 문장을 추가한다.

```python
from middlewares.exception import ExceptionHandlerMiddleware

...

app.add_middleware(ExceptionHandlerMiddleware)
```

그리고 짜잔! 이 멋진 설정을 사용하면 모든 예외가 손쉽게 처리되어 응답으로 반환되므로, 좋아하는 밈 컬렉션처럼 쉽게 볼 수 있고 액세스할 수 있다. 더 이상 어려운 로그를 뒤지거나 찾기 어려운 버그를 쫓아다니지 않아도 된다.

이제 편안히 앉아 긴장을 풀고 오류 없는 어플리케이션으로 순조로운 항해를 즐기세요!

**이제 모든 버그가 제거되고 코딩 은하계에서 초신성보다 더 밝게 빛날 준비가 되었다! 이것이 바로 프로덕션 준비가 완료된 코드이다.**

```python
import logging
from fastapi import Request, HTTPException
from fastapi.responses import JSONResponse
from starlette.middleware.base import BaseHTTPMiddleware

logger = logging.getLogger(__name__)

class ExceptionHandlerMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request: Request, call_next):
        try:
            return await call_next(request)
        except HTTPException as http_exception:
            return JSONResponse(
                status_code=http_exception.status_code,
                content={"error": "Client Error", "message": str(http_exception.detail)},
            )
        except Exception as e:
            logger.exception(msg=e.__class__.__name__, args=e.args)
            return JSONResponse(
                status_code=500,
                content={"error": "Internal Server Error", "message": "An unexpected error occurred."},
            )
```
